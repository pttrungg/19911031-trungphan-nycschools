package usa.chase.nycschools.repository.room

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import usa.chase.nycschools.di.*
import usa.chase.nycschools.model.remote.response.School
import usa.chase.nycschools.model.repository.room.SchoolRoomRepository

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolRoomRepositoryTest : KoinTest {

    private val schoolRoomRepository : SchoolRoomRepository by inject()

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Before
    @Throws(Exception::class)
    fun before() {
        MockitoAnnotations.initMocks(this)
        stopKoin()

        startKoin {
            modules(
                viewModelsModuleTest,
                roomModuleTest,
                repositoryModuleTest,
                apiModuleTest,
                networkModuleTest

            )
        }
    }

    @After
    @Throws(Exception::class)
    fun after() {
        stopKoin()
    }

    @Test
    fun insert() {
        val school1 = School(dbn = "--", schoolName = "--", overviewParagraph = "--", location = "--", phoneNumber = "--", faxNumber = "--", schoolEmail = "--", website = "--", latitude = "--", longitude = "--")
        runBlocking {
            schoolRoomRepository.insert(school1).let {
                Truth.assertThat(it).isNotNull()
            }
        }
    }

    @Test
    fun insert_all() {
        val school1 = School(dbn = "--", schoolName = "--", overviewParagraph = "--", location = "--", phoneNumber = "--", faxNumber = "--", schoolEmail = "--", website = "--", latitude = "--", longitude = "--")
        val school2 = School(dbn = "--", schoolName = "--", overviewParagraph = "--", location = "--", phoneNumber = "--", faxNumber = "--", schoolEmail = "--", website = "--", latitude = "--", longitude = "--")
        runBlocking {
            schoolRoomRepository.insertAll(listOf(school1, school2)).let {
                Truth.assertThat(it).isNotNull()
            }
        }
    }

    @Test
    fun delete_all() {
        runBlocking {
            schoolRoomRepository.deleteAll().let {
                Truth.assertThat(it).isNotNull()
            }
        }
    }
}