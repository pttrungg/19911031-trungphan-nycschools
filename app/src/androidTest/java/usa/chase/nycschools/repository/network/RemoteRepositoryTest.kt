package usa.chase.nycschools.repository.network

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import usa.chase.nycschools.di.*
import usa.chase.nycschools.model.repository.remote.RemoteRepository

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RemoteRepositoryTest : KoinTest {

    private val remoteRepository: RemoteRepository by inject()

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Before
    @Throws(Exception::class)
    fun before() {
        MockitoAnnotations.initMocks(this)
        stopKoin()

        startKoin {
            modules(
                viewModelsModuleTest,
                roomModuleTest,
                repositoryModuleTest,
                apiModuleTest,
                networkModuleTest

            )
        }
    }

    @After
    @Throws(Exception::class)
    fun after() {
        stopKoin()
    }

    @Test
    fun get_schools() {
        runBlocking {
            remoteRepository.getSchools().let {
                Truth.assertThat(it.data).isNotNull()
            }
        }
    }

    @Test
    fun get_scores() {
        runBlocking {
            remoteRepository.getScores().let {
                Truth.assertThat(it.data).isNotNull()
            }
        }
    }

    @Test
    fun get_score_by_dbn() {
        runBlocking {
            remoteRepository.getScoreByDbn("02M047").let {
                Truth.assertThat(it.data).isNotNull()
            }
        }
    }
}