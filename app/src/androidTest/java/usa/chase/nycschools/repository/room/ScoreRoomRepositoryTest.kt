package usa.chase.nycschools.repository.room

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import usa.chase.nycschools.di.*
import usa.chase.nycschools.model.remote.response.Score
import usa.chase.nycschools.model.repository.room.ScoreRoomRepository

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ScoreRoomRepositoryTest : KoinTest {

    private val scoreRoomRepository : ScoreRoomRepository by inject()

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Before
    @Throws(Exception::class)
    fun before() {
        MockitoAnnotations.initMocks(this)
        stopKoin()

        startKoin {
            modules(
                viewModelsModuleTest,
                roomModuleTest,
                repositoryModuleTest,
                apiModuleTest,
                networkModuleTest

            )
        }
    }

    @After
    @Throws(Exception::class)
    fun after() {
        stopKoin()
    }


    @Test
    fun insert_all() {
        val score1 = Score(dbn = "--", testTakersNo = "--", readingAvgScore = "--", mathAvgScore = "--", writingAvgScore = "--", schoolName = "--")
        val score2 = Score(dbn = "--", testTakersNo = "--", readingAvgScore = "--", mathAvgScore = "--", writingAvgScore = "--", schoolName = "--")
        val score3 = Score(dbn = "--", testTakersNo = "--", readingAvgScore = "--", mathAvgScore = "--", writingAvgScore = "--", schoolName = "--")
        runBlocking {
            scoreRoomRepository.insertAll(listOf(score1, score2, score3)).let {
                Truth.assertThat(it).isNotNull()
            }
        }
    }

    @Test
    fun get_all() {
        runBlocking {
            scoreRoomRepository.getAll().let {
                Truth.assertThat(it).isNotNull()
            }
        }
    }

    @Test
    fun delete_all() {
        runBlocking {
            scoreRoomRepository.deleteAll().let {
                Truth.assertThat(it).isNotNull()
            }
        }
    }

    @Test
    fun get_by_dbn() {
        runBlocking {
            scoreRoomRepository.getByDbn("--").let {
                Truth.assertThat(it).isNotNull()
            }
        }
    }
}