package usa.chase.nycschools.di

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import usa.chase.nycschools.model.remote.api.ApiService
import usa.chase.nycschools.model.repository.remote.RemoteRepository
import usa.chase.nycschools.model.repository.remote.RemoteRepositoryImpl
import usa.chase.nycschools.model.repository.room.SchoolRoomRepository
import usa.chase.nycschools.model.repository.room.SchoolRoomRepositoryImpl
import usa.chase.nycschools.model.repository.room.ScoreRoomRepository
import usa.chase.nycschools.model.repository.room.ScoreRoomRepositoryImpl
import usa.chase.nycschools.model.room.NYCSchoolsDatabase
import usa.chase.nycschools.ui.activity.MainViewModel
import usa.chase.nycschools.ui.fragment.schoolslist.SchoolsListViewModel
import usa.chase.nycschools.ui.fragment.score.ScoreViewModel
import usa.chase.nycschools.util.ResponseHandler

val testDispatcher = module {
    factory { TestCoroutineDispatcher() }
}

val viewModelsModuleTest = module {
    viewModel { MainViewModel(get(), get(), get()) }
    viewModel { SchoolsListViewModel(get(),get(), get()) }
    viewModel { ScoreViewModel(get(), get(), get()) }
}

val networkModuleTest = module {
    factory { buildOkHttpClient() }
    single { buildRetrofit(get()) }
    factory { ResponseHandler() }
}

val roomModuleTest = module {
    single {
        // In-Memory database config
        Room.databaseBuilder(
            ApplicationProvider.getApplicationContext<Context>(), NYCSchoolsDatabase::class.java,
            "db"
        ).allowMainThreadQueries().build()
    }
    single(createdAtStart = false) { get<NYCSchoolsDatabase>().schoolDao() }
    single(createdAtStart = false) { get<NYCSchoolsDatabase>().scoreDao() }
}

val apiModuleTest = module {
    single(createdAtStart = false) { get<Retrofit>().create(ApiService::class.java) }
}

val repositoryModuleTest = module {
    factory<RemoteRepository> { RemoteRepositoryImpl(get()) }
    factory<SchoolRoomRepository> { SchoolRoomRepositoryImpl(get()) }
    factory<ScoreRoomRepository> { ScoreRoomRepositoryImpl(get()) }
}