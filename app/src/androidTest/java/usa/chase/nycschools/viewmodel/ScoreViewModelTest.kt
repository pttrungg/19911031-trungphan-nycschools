package usa.chase.nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import usa.chase.nycschools.di.*
import usa.chase.nycschools.model.repository.remote.RemoteRepository
import usa.chase.nycschools.model.repository.room.ScoreRoomRepository
import usa.chase.nycschools.ui.fragment.score.ScoreViewModel

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ScoreViewModelTest : KoinTest {

    private val testDispatcher = TestCoroutineDispatcher()
    private val remoteRepository: RemoteRepository by inject()
    private val scoreRoomRepository: ScoreRoomRepository by inject()

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()


    @Before
    @Throws(Exception::class)
    fun before() {
        MockitoAnnotations.initMocks(this)
        stopKoin()

        startKoin {
            modules(
                viewModelsModuleTest,
                roomModuleTest,
                repositoryModuleTest,
                apiModuleTest,
                networkModuleTest

            )
        }
    }

    @After
    @Throws(Exception::class)
    fun after() {
        stopKoin()
    }

    @Test
    fun get_score_by_dbn() {
        runBlocking {
            val vm = ScoreViewModel(testDispatcher, scoreRoomRepository , remoteRepository)
            vm.getScoreByDbnFromRoom("01M696")
            delay(2000)
            Truth.assertThat(vm.score.get()).isNotNull()
        }
    }
}