package usa.chase.nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.MockitoAnnotations
import usa.chase.nycschools.di.*
import usa.chase.nycschools.model.repository.remote.RemoteRepository
import usa.chase.nycschools.model.repository.room.ScoreRoomRepository
import usa.chase.nycschools.ui.activity.MainViewModel
import org.koin.test.inject
import org.koin.test.KoinTest

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest : KoinTest {
    private val testDispatcher = TestCoroutineDispatcher()
    private val scoreRoomRepository by inject<ScoreRoomRepository>()
    private val remoteRepository by inject<RemoteRepository>()

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()


    @Before
    @Throws(Exception::class)
    fun before() {
        MockitoAnnotations.initMocks(this)
        stopKoin()

        startKoin {
            modules(
                viewModelsModuleTest,
                roomModuleTest,
                repositoryModuleTest,
                apiModuleTest,
                networkModuleTest

            )
        }
    }

    @After
    @Throws(Exception::class)
    fun after() {
        stopKoin()
    }

    /**
     * Test with board_id is available
     */
    @Test
    fun get_all_scores_from_network_and_save_to_room() {
        runBlocking {
            val mainViewModel = MainViewModel(testDispatcher, scoreRoomRepository, remoteRepository)
            mainViewModel.getAllScoresFromNetWorkToRoom()
            delay(3000)
            mainViewModel.getAllScoresFromRoom()
            delay(2000)
            Truth.assertThat(mainViewModel.scoreTest.value).isNotEmpty()
        }
    }
}