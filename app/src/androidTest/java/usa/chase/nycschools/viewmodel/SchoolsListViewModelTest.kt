package usa.chase.nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import usa.chase.nycschools.di.*
import usa.chase.nycschools.model.repository.remote.RemoteRepository
import usa.chase.nycschools.model.repository.room.SchoolRoomRepository
import usa.chase.nycschools.ui.fragment.schoolslist.SchoolsListViewModel

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolsListViewModelTest : KoinTest {

    private val testDispatcher = TestCoroutineDispatcher()
    private val remoteRepository: RemoteRepository by inject()
    private val schoolRoomRepository: SchoolRoomRepository by inject()

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()


    @Before
    @Throws(Exception::class)
    fun before() {
        MockitoAnnotations.initMocks(this)
        stopKoin()

        startKoin {
            modules(
                viewModelsModuleTest,
                roomModuleTest,
                repositoryModuleTest,
                apiModuleTest,
                networkModuleTest

            )
        }
    }

    @After
    @Throws(Exception::class)
    fun after() {
        stopKoin()
    }

    @Test
    fun get_all_schools_from_network_and_save_to_room() {
        runBlocking {
            val vm = SchoolsListViewModel(testDispatcher, schoolRoomRepository, remoteRepository)
            vm.getSchoolListFromRoom()
            delay(2000)
            vm.getAllSchoolsFromRoom()
            delay(1000)
            Truth.assertThat(vm.schoolListTest.value).isNotEmpty()
        }
    }
}