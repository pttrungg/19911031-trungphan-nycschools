package usa.chase.nycschools.ui.fragment.score

import android.widget.Toast
import androidx.navigation.fragment.navArgs
import org.koin.android.viewmodel.ext.android.viewModel
import usa.chase.nycschools.R
import usa.chase.nycschools.base.BaseFragment
import usa.chase.nycschools.databinding.FragmentScoreBinding

class ScoreFragment : BaseFragment<FragmentScoreBinding>() {

    private val viewModel: ScoreViewModel by viewModel()
    private val args: ScoreFragmentArgs by navArgs()

    override fun layoutResId() = R.layout.fragment_score

    companion object {
        fun newInstance() = ScoreFragment()
    }

    override fun viewDidLoad() {
        initData()
        getExtraDataFromLogin()
        observeData()
    }

    private fun observeData() {
        viewModel.error.observe(viewLifecycleOwner, {
            it?.let {
                if (it.contains("2147483647")) {
                    Toast.makeText(requireContext(),  "NO INTERNET CONNECTION\nConnect with Internet to get the updated data!", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun initData() {
        binding.vm = viewModel
    }

    private fun getExtraDataFromLogin() {
        args.school?.let {
            viewModel.school.set(it)
            viewModel.getScoreByDbnFromRoom(it.dbn)
        }
    }

}