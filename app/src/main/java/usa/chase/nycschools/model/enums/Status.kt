package usa.chase.nycschools.model.enums

enum class Status {
    SUCCESS,
    ERROR
}