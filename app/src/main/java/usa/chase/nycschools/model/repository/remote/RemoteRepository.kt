package usa.chase.nycschools.model.repository.remote

import usa.chase.nycschools.base.BaseApiResult
import usa.chase.nycschools.model.remote.response.School
import usa.chase.nycschools.model.remote.response.Score

interface RemoteRepository {

    suspend fun getSchools() : BaseApiResult<List<School>>

    suspend fun getScores() : BaseApiResult<List<Score>>

    suspend fun getScoreByDbn(dbn: String) : BaseApiResult<List<Score>>
}