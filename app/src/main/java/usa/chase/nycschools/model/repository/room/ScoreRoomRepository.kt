package usa.chase.nycschools.model.repository.room

import usa.chase.nycschools.model.remote.response.Score

interface ScoreRoomRepository {

    suspend fun getByDbn(dbn: String) : Score?

    suspend fun insertAll(listScore: List<Score>) : List<Long>?

    suspend fun deleteAll() : Int?

    suspend fun getAll(): List<Score?>?
}