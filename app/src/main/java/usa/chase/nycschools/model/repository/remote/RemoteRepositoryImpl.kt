package usa.chase.nycschools.model.repository.remote

import usa.chase.nycschools.base.BaseApiResult
import usa.chase.nycschools.base.BaseRepository
import usa.chase.nycschools.model.remote.api.ApiService
import usa.chase.nycschools.model.remote.response.School
import usa.chase.nycschools.model.remote.response.Score
import usa.chase.nycschools.model.repository.remote.RemoteRepository

class RemoteRepositoryImpl(private val apiService: ApiService) : RemoteRepository, BaseRepository() {
    override suspend fun getSchools(): BaseApiResult<List<School>> {
        return safeApi {
            apiService.getSchools()
        }
    }

    override suspend fun getScores(): BaseApiResult<List<Score>> {
        return safeApi {
            apiService.getScores()
        }
    }

    override suspend fun getScoreByDbn(dbn: String): BaseApiResult<List<Score>> {
        return safeApi {
            apiService.getScoreByDbn(dbn)
        }
    }
}