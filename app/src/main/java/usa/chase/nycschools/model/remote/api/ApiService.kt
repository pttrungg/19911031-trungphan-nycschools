package usa.chase.nycschools.model.remote.api

import retrofit2.http.GET
import retrofit2.http.Query
import usa.chase.nycschools.model.remote.response.School
import usa.chase.nycschools.model.remote.response.Score

interface ApiService {
    @GET("s3k6-pzi2.json")
    suspend fun getSchools(): List<School>

    @GET("f9bf-2cp4.json")
    suspend fun getScores(): List<Score>

    @GET("f9bf-2cp4.json")
    suspend fun getScoreByDbn(@Query("dbn") dbn: String): List<Score>
}