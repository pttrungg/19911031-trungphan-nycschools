package usa.chase.nycschools.model.enums

enum class SharedPreferenceKey {
    ACCESS_TOKEN
}