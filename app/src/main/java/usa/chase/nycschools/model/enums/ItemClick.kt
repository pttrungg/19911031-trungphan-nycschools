package usa.chase.nycschools.model.enums

enum class ItemClick {
    PARENT,
    PHONE,
    MAP
}