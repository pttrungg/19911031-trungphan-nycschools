package usa.chase.nycschools

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import usa.chase.nycschools.di.*
import usa.chase.nycschools.util.SharedPreferences.initSharedPreferences

class NYCSchoolsApp : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

        initSharedPreferences(this.applicationContext)

        startKoin{
            androidLogger()
            androidContext(this@NYCSchoolsApp)
            modules(listOf(viewModelsModule, networkModule, repositoryModule, apiModule, roomModule))
        }
    }

    companion object {
        lateinit var INSTANCE: NYCSchoolsApp
    }
}