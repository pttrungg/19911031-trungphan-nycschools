package usa.chase.nycschools.di

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import usa.chase.nycschools.model.room.NYCSchoolsDatabase

val roomModule = module {
    single {
        NYCSchoolsDatabase(androidApplication())
    }
    single(createdAtStart = false) { get<NYCSchoolsDatabase>().schoolDao() }
    single(createdAtStart = false) { get<NYCSchoolsDatabase>().scoreDao() }
}