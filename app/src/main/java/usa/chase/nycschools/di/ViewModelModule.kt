package usa.chase.nycschools.di

import kotlinx.coroutines.Dispatchers
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import usa.chase.nycschools.model.repository.remote.RemoteRepository
import usa.chase.nycschools.model.repository.remote.RemoteRepositoryImpl
import usa.chase.nycschools.model.repository.room.SchoolRoomRepository
import usa.chase.nycschools.model.repository.room.SchoolRoomRepositoryImpl
import usa.chase.nycschools.model.repository.room.ScoreRoomRepository
import usa.chase.nycschools.model.repository.room.ScoreRoomRepositoryImpl
import usa.chase.nycschools.ui.activity.MainViewModel
import usa.chase.nycschools.ui.fragment.schoolslist.SchoolsListViewModel
import usa.chase.nycschools.ui.fragment.score.ScoreViewModel

val viewModelsModule = module {
    factory { Dispatchers.IO }
    viewModel { MainViewModel(get(), get(), get()) }
    viewModel { SchoolsListViewModel(get(),get(), get()) }
    viewModel { ScoreViewModel(get(), get(), get()) }
}