package usa.chase.nycschools.di

import org.koin.dsl.module
import retrofit2.Retrofit
import usa.chase.nycschools.model.remote.api.ApiService

val apiModule = module {
    single(createdAtStart = false) { get<Retrofit>().create(ApiService::class.java) }
}