package usa.chase.nycschools.di

import org.koin.dsl.module
import usa.chase.nycschools.model.repository.remote.RemoteRepository
import usa.chase.nycschools.model.repository.remote.RemoteRepositoryImpl
import usa.chase.nycschools.model.repository.room.SchoolRoomRepository
import usa.chase.nycschools.model.repository.room.SchoolRoomRepositoryImpl
import usa.chase.nycschools.model.repository.room.ScoreRoomRepository
import usa.chase.nycschools.model.repository.room.ScoreRoomRepositoryImpl

val repositoryModule = module {
    factory<RemoteRepository> { RemoteRepositoryImpl(get()) }
    factory<SchoolRoomRepository> { SchoolRoomRepositoryImpl(get()) }
    factory<ScoreRoomRepository> { ScoreRoomRepositoryImpl(get()) }
}