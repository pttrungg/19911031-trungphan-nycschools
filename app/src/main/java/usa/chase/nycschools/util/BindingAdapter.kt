package usa.chase.nycschools.util

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import usa.chase.nycschools.model.remote.response.School
import usa.chase.nycschools.ui.fragment.schoolslist.SchoolListAdapter
import usa.chase.nycschools.util.extension.hideKeyboard
import usa.chase.nycschools.util.extension.showKeyboard

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<School>?) {
    val adapter = recyclerView.adapter as SchoolListAdapter
    data?.let {
        adapter.setData(data.toMutableList())
    }
}

@BindingAdapter("filterByKeyword")
fun filterByKeyword(recyclerView: RecyclerView, keyword: String?) {
    val adapter = recyclerView.adapter as SchoolListAdapter
    keyword?.let {
        adapter.filter.filter(it)
    }
}

@BindingAdapter("hideKeyboard")
fun hideKeyboardOnclick(view: View, isHide: Boolean) {
    if (isHide)
        view.hideKeyboard()
}

@BindingAdapter("showKeyboard")
fun showKeyboardOnclick(view: View, isShow: Boolean) {
    if (isShow)
        view.showKeyboard()
}