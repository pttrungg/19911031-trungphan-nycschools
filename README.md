# 19911031-TrungPhan-NYCSchools
## Feature
- Search school by name
- Reading offline after the 1st time using APP
- Navigate to the address of the school using Google Map
- Make a phone call to the School

## Tech Stack Used
- Koin for DI
- Coroutine for asynchronus prrogramming
- MVVM
- Retrofit2 
- okHTTP
- ROOM DB
- Navigation Component
- ViewModel
- DataBinding
- Unit/Integration Test
